from datetime import datetime
from decimal import Decimal

from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.views.generic import FormView, ListView, TemplateView
from django.views.generic.list import MultipleObjectTemplateResponseMixin

from .forms import BulkImportForm, DisbursementBulkImportForm
from .models import Disbursement, Optransactionhistoryux3, LastUpdated

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.contrib import messages


@login_required()
def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect("app:home")
        else:
            return render(request, 'registration/register.html', {'form': form})

    else:
        form = UserCreationForm()
        return render(request, 'registration/register.html', {'form': form})


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'app/home.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        last_updated = LastUpdated.objects.all()
        context["lens"] = last_updated.filter(table_name="LENSCROW")[
            0] if last_updated.filter(table_name="LENSCROW") else None

        context["dis"] = last_updated.filter(table_name="DISBURSEMENT")[
            0] if last_updated.filter(table_name="DISBURSEMENT") else None

        return self.render_to_response(context)


class LenscrowDataListView(LoginRequiredMixin, View):
    template_name = "app/lenscrow-data-list.html"

    def get_list(self, request):
        obj = Optransactionhistoryux3.objects.all()
        filter_val = request.GET.get("q")
        # import pdb; pdb.set_trace()
        if filter_val:
            if filter_val == "data":
                obj = obj.filter(is_duplicate=False)

            elif filter_val == "duplicates":
                obj = obj.filter(is_duplicate=True)
        else:
            obj = Optransactionhistoryux3.objects.filter(is_duplicate=False)

        paginator = Paginator(obj, 20)
        page_number = request.GET.get('page')
        return paginator.get_page(page_number)

    def get_filter_val(self, request):
        return self.request.GET.get("q")

    def get(self, request, *args, **kwargs):
        form = BulkImportForm()
        context = {
            "object_list": self.get_list(request),
            "form": form,
            "filter_val": self.get_filter_val(request)
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = BulkImportForm(request.POST, request.FILES)
        form_error = False
        duplicates = []

        list_objs = Optransactionhistoryux3.objects.filter(
            is_duplicate=False).values_list('transaction_id', flat=True)
        if form.is_valid():

            df = form.data_frame
            data = []
            for index, row in df.iterrows():
                if row["Transaction ID"] not in list_objs:
                    data.append(
                        Optransactionhistoryux3(
                            transaction_id=row["Transaction ID"],
                            value_date=datetime.strptime(
                                row["Value Date"], '%d/%m/%Y'),
                            txn_posted_date=datetime.strptime(
                                row["Txn Posted Date"].strip(), '%d/%m/%Y %I:%M:%S %p'),
                            cheque_no=row["ChequeNo."],
                            description=row["Description"],
                            cr_dr=row["Cr/Dr"],
                            txn_amount=Decimal(row["Transaction Amount(INR)"]),
                            available_balance=Decimal(
                                row["Available Balance(INR)"])
                        ),
                    )
                else:
                    duplicates.append(
                        Optransactionhistoryux3(
                            transaction_id=row["Transaction ID"],
                            value_date=datetime.strptime(
                                row["Value Date"], '%d/%m/%Y'),
                            txn_posted_date=datetime.strptime(
                                row["Txn Posted Date"].strip(), '%d/%m/%Y %I:%M:%S %p'),
                            cheque_no=row["ChequeNo."],
                            description=row["Description"],
                            cr_dr=row["Cr/Dr"],
                            txn_amount=Decimal(row["Transaction Amount(INR)"]),
                            available_balance=Decimal(
                                row["Available Balance(INR)"]),
                            is_duplicate=True,
                        ),
                    )

            Optransactionhistoryux3.objects.bulk_create(data)
            Optransactionhistoryux3.objects.bulk_create(duplicates)

            obj, created = LastUpdated.objects.get_or_create(
                table_name="LENSCROW")
            obj.last_updated_datetime = timezone.now()
            obj.save()

            if duplicates:
                message = "{} rows duplicated.".format(len(duplicates))
                messages.info(request, message)

            return redirect("app:lenscrow_data_list")
        else:
            form_error = True

        context = {
            "form": form,
            "form_error": form_error,
            "object_list": self.get_list(request),
            # "duplicates": len(duplicates)
        }
        return render(request, self.template_name, context)

class DisbursementDataListView(LoginRequiredMixin, View):
    template_name = "app/disbursement-data-list.html"

    def get_list(self, request):
        obj = Disbursement.objects.filter(is_duplicate=False)

        paginator = Paginator(obj, 20)
        page_number = request.GET.get('page')
        return paginator.get_page(page_number)

    def get(self, request, *args, **kwargs):
        form = BulkImportForm()
        context = {"object_list": self.get_list(request), "form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = DisbursementBulkImportForm(request.POST, request.FILES)

        form_error = False
        duplicates = []

        custref_list = Disbursement.objects.filter(
            is_duplicate=False
        ).values_list('cust_ref_no', flat=True)

        cmsref_list = Disbursement.objects.filter(is_duplicate=False
                                                  ).values_list('cms_ref_no', flat=True)

        if form.is_valid():

            df = form.data_frame
            data = []
            for index, row in df.iterrows():
                if row["CUST_REFNO"] not in custref_list and row["CMS Ref. No."] not in cmsref_list:
                    try:
                        instruction_date = datetime.strptime(
                            row["Instruction_DATE"], "%d-%m-%Y")
                    except:
                        instruction_date = None

                    try:
                        liquidation_date = datetime.strptime(
                            str(row["Liquidation Date"]), "%d-%m-%Y")
                    except:
                        liquidation_date = None

                    data.append(
                        Disbursement(
                            client_code=row["CLIENT_CODE"],
                            debit_ac_no=row["DEBIT_AC_NO"],
                            tran_code=row["TRAN_CODE"],
                            instruction_date=instruction_date,
                            amount=Decimal(row["AMOUNT"]),
                            bene_name=row["BENE_NAME"],
                            bene_account_no=row["BENE_Account_no."],
                            ifsc_code=row["IFSC_code"],
                            cust_ref_no=row["CUST_REFNO"],
                            # cust_ref_no = row["CLIENT_CODE"],
                            beneficiary_email_id=row["Beneficiary_email_id"],
                            beneficiary_mobile_no=row["beneficiary_mobile_no."],
                            remarks=row["Remarks"],
                            transaction_status=row["Transaction Status"],
                            cms_ref_no=row["CMS Ref. No."],
                            utr_no=row["UTR No."],
                            liquidation_date=liquidation_date,
                            rejection_reason=row["Rejection Reason"],
                            maker=row["Maker"],
                            authorizer1=row["Authorizer1"],
                            authorizer2=row["Authorizer2"]
                        ),
                    )
                    # except:
                    #     pass

                else:
                    try:
                        instruction_date = datetime.strptime(
                            row["Instruction_DATE"], "%d-%m-%Y")
                    except:
                        instruction_date = None

                    try:
                        liquidation_date = datetime.strptime(
                            str(row["Liquidation Date"]), "%d-%m-%Y")
                    except:
                        liquidation_date = None

                    duplicates.append(
                        Disbursement(
                            client_code=row["CLIENT_CODE"],
                            debit_ac_no=row["DEBIT_AC_NO"],
                            tran_code=row["TRAN_CODE"],
                            instruction_date=instruction_date,
                            amount=Decimal(row["AMOUNT"]),
                            bene_name=row["BENE_NAME"],
                            bene_account_no=row["BENE_Account_no."],
                            ifsc_code=row["IFSC_code"],
                            cust_ref_no=row["CUST_REFNO"],
                            # cust_ref_no = row["CLIENT_CODE"],
                            beneficiary_email_id=row["Beneficiary_email_id"],
                            beneficiary_mobile_no=row["beneficiary_mobile_no."],
                            remarks=row["Remarks"],
                            transaction_status=row["Transaction Status"],
                            cms_ref_no=row["CMS Ref. No."],
                            utr_no=row["UTR No."],
                            liquidation_date=liquidation_date,
                            rejection_reason=row["Rejection Reason"],
                            maker=row["Maker"],
                            authorizer1=row["Authorizer1"],
                            authorizer2=row["Authorizer2"],
                            is_duplicate=True,
                        ),
                    )

            Disbursement.objects.bulk_create(data)
            Disbursement.objects.bulk_create(duplicates)

            obj, created = LastUpdated.objects.get_or_create(
                table_name="DISBURSEMENT")
            obj.last_updated_datetime = timezone.now()
            obj.save()

            if duplicates:
                message = "{} rows duplicated.".format(len(duplicates))
                messages.info(request, message)

            return redirect("app:disbursement_data_list")

        else:
            form_error = True

        context = {
            "form": form,
            "form_error": form_error,
            "object_list": self.get_list(request),
            "duplicates": len(duplicates)}
        return render(request, self.template_name, context)
