from django.urls import path
from .views import LenscrowDataListView, DisbursementDataListView, HomeView, register

app_name = "app"

urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('register/', register, name='register'),
    path('lenscrow/list', LenscrowDataListView.as_view(),
         name="lenscrow_data_list"),

#     path('disbursement/import', DisbursementDataImportView.as_view(),
#          name="disbursement_data_import"),
    path('disbursement/list', DisbursementDataListView.as_view(),
         name="disbursement_data_list"),
]
