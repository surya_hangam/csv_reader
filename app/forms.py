import pandas
from django import forms


class DisbursementBulkImportForm(forms.Form):
    
    # file = forms.FileField()
    file = forms.FileField(widget=forms.FileInput(attrs={'class': 'from-control'}))

    def clean_file(self):
        file_name = self.cleaned_data.get("file")

        file = self.files.get('file')

        if file_name._name.split('.')[-1] != 'csv':
            raise forms.ValidationError('Please, upload a csv file!')

        try:
            df = pandas.read_csv(file, delimiter=',', encoding='utf-8')
            df.drop_duplicates(
                subset=["CUST_REFNO", "CMS Ref. No."],
                keep='first', 
                inplace=True)
            df = pandas.DataFrame(df)
            # df = pandas.DataFrame(pandas.read_csv(file))
        except:
            raise forms.ValidationError('Could not read the file')
        self.data_frame = df

        return file

class BulkImportForm(forms.Form):
    file = forms.FileField()

    def clean_file(self):
        file_name = self.cleaned_data.get("file")

        file = self.files.get('file')

        if file_name._name.split('.')[-1] != 'csv':
            raise forms.ValidationError('Please, upload a csv file!')

        try:
            df = pandas.read_csv(file, delimiter=',', encoding='utf-8')
            df.drop_duplicates(subset="Transaction ID", inplace=True)
            df = pandas.DataFrame(df)
            # df = pandas.DataFrame(pandas.read_csv(file))
        except:
            raise forms.ValidationError('Could not read the file')
        self.data_frame = df

        return file
