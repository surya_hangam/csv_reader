from statistics import mode
from unicodedata import decimal

from django.db import models

class LastUpdated(models.Model):
    table_name = models.CharField(max_length=100)
    last_updated_datetime = models.DateTimeField(null=True)

class Optransactionhistoryux3(models.Model):
    transaction_id = models.CharField(
        max_length=255)
    value_date = models.DateField(blank=True, null=True)
    txn_posted_date = models.DateTimeField(blank=True, null=True)
    cheque_no = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255,  blank=True, null=True)
    cr_dr = models.CharField(max_length=10, blank=True, null=True)
    txn_amount = models.DecimalField(
        max_digits=18, decimal_places=0, blank=True, null=True)
    available_balance = models.DecimalField(
        max_digits=18, decimal_places=0, blank=True, null=True)
    is_duplicate = models.BooleanField(default=False)

    def __str__(self):
        return self.transaction_id


class Disbursement(models.Model):
    client_code = models.CharField(max_length=255)
    debit_ac_no = models.CharField(max_length=255)
    tran_code = models.CharField(max_length=255)
    instruction_date = models.DateField(null=True)
    amount = models.DecimalField(max_digits=15, decimal_places=2)
    bene_name = models.CharField(max_length=255)
    bene_account_no = models.CharField(max_length=255)
    ifsc_code = models.CharField(max_length=255)
    cust_ref_no = models.CharField(max_length=255)
    beneficiary_email_id = models.EmailField(null=True)
    beneficiary_mobile_no = models.CharField(max_length=255, null=True)
    remarks = models.TextField(null=True)
    transaction_status = models.CharField(max_length=255)
    cms_ref_no = models.CharField(max_length=255)
    utr_no = models.CharField(max_length=255)
    liquidation_date = models.DateField(null=True)
    rejection_reason = models.TextField(null=True)
    maker = models.CharField(max_length=255)
    authorizer1 = models.CharField(max_length=255, null=True)
    authorizer2 = models.CharField(max_length=255, null=True)
    is_duplicate = models.BooleanField(default=False)


    def __str__(self):
        return self.client_code