from django.contrib import admin
from .models import Disbursement, Optransactionhistoryux3, LastUpdated

admin.site.register(Disbursement)
admin.site.register(Optransactionhistoryux3)
admin.site.register(LastUpdated)

