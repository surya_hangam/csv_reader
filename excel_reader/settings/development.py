from .base import *

ALLOWED_HOSTS = []
STATIC_URL = '/static/'
MEDIA_URL = '/media/'
DEBUG = True
STATICFILES_DIRS = [
    os.path.join(os.path.dirname(BASE_DIR), "static")
]