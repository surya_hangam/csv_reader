
from .base import *

ALLOWED_HOSTS = ['*']
DEBUG = True
STATIC_ROOT = "/home/ubuntu/csv_reader/static_cdn"
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media")
STATIC_URL = '/static/'
MEDIA_URL = '/media/'
CSRF_TRUSTED_ORIGINS = ['https://escrow.lendenclub.com']
